package me.zyrkran.thehub;

import java.util.HashMap;

public class DataHandler{

    private static HashMap<String, Boolean> status = new HashMap<>();
    private static HashMap<String, Integer> players = new HashMap<>();

    public static void updateCount(String server, int amount){
        players.put(server, amount);
    }

    public static int getPlayerCount(String server){
        if (!players.containsKey(server)) return 0;
        return players.get(server);
    }

    public static void updateStatus(String server, boolean bool){
        status.put(server, bool);
    }

    public static boolean getServerStatus(String server){
        if (!status.containsKey(server)) return false;
        return status.get(server);
    }



}
