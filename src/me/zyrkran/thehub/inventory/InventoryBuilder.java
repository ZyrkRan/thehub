package me.zyrkran.thehub.inventory;

import me.zyrkran.thehub.Main;
import me.zyrkran.thehub.ServerEnum;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class InventoryBuilder {

    private static Inventory inventory_nav;
    private static Inventory inventory_player;

    private static ItemStack item_navigator;

    public InventoryBuilder() {
        inventory_nav = Bukkit.createInventory(null, 9, "Server Selector");

        // update the inventory every second
        Bukkit.getScheduler().runTaskTimerAsynchronously(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                // loop //
                for (ServerEnum serverEnum : ServerEnum.values()) {
                    ItemStack item = new ItemStack(serverEnum.getMaterial());
                    ItemMeta meta = item.getItemMeta();

                    meta.setDisplayName(serverEnum.getDisplayName());
                    meta.setLore(serverEnum.getLore());
                    meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_DESTROYS, ItemFlag.HIDE_UNBREAKABLE);
                    item.setItemMeta(meta);

                    inventory_nav.setItem(serverEnum.getSlot(), item);
                }
            }
        }, 0, 20);

        inventory_player = Bukkit.createInventory(null, 36, "Player inventory");

        item_navigator = new ItemStack(Material.COMPASS);
        ItemMeta meta = item_navigator.getItemMeta();

        meta.setDisplayName(ChatColor.GOLD + "Navigator");
        item_navigator.setItemMeta(meta);

        inventory_player.setItem(4, item_navigator);
    }

    public static ItemStack getNavigatorItem(){
        return item_navigator;
    }

    public static Inventory getNavigatorInventory(){
        return inventory_nav;
    }

    public static Inventory getPlayerInventory(){
        return inventory_player;
    }
}
