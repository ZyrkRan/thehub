package me.zyrkran.thehub.listeners;

import fr.rhaz.socket4mc.Bukkit.BukkitSocketHandshakeEvent;
import fr.rhaz.socket4mc.Bukkit.BukkitSocketJSONEvent;
import fr.rhaz.socketapi.SocketAPI.Client.SocketClient;
import me.zyrkran.thehub.DataHandler;
import me.zyrkran.thehub.Main;
import me.zyrkran.thehub.ServerEnum;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SocketManager implements Listener{

    public static SocketClient CLIENT;

    static {

        // server status refresh
        Bukkit.getScheduler().runTaskTimerAsynchronously(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (CLIENT != null){
                    for (ServerEnum server : ServerEnum.values()){
                        writeJSON("TheHub", "SERVER_STATUS_REQUEST;" + server);
                    }
                }
            }

        }, 5, 100);

        // player count refresh
        Bukkit.getScheduler().runTaskTimerAsynchronously(Main.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (CLIENT != null){
                    for (ServerEnum server : ServerEnum.values()){
                       writeJSON("TheHub", "PLAYERCOUNT_REQUEST;" + server);
                    }

                }

            }

        },5, 20);

    }

    public static void writeJSON(String channel, String data){
        CLIENT.writeJSON(channel, data);
    }

    @EventHandler
    public void onHandshake(BukkitSocketHandshakeEvent event){
        CLIENT = event.getClient();
    }

    @EventHandler
    public void onJSON(BukkitSocketJSONEvent event){
        String channel = event.getChannel();

        if (channel.equalsIgnoreCase("TheHub")){

            String[] data = event.getData().split(";");
            if (data[0].equalsIgnoreCase("PLAYERCOUNT_RESPONSE")){
                String server = data[1];
                int amount = Integer.parseInt(data[2]);

                // updateCount data
                DataHandler.updateCount(server, amount);
            }

            else if (data[0].equalsIgnoreCase("SERVER_STATUS_RESPONSE")){
                String server = data[1];
                boolean bool = Boolean.parseBoolean(data[2]);

                DataHandler.updateStatus(server, bool);
            }
        }
    }
}
