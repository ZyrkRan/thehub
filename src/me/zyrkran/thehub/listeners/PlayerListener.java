package me.zyrkran.thehub.listeners;

import me.zyrkran.thehub.inventory.InventoryBuilder;
import me.zyrkran.thehub.Main;
import me.zyrkran.thehub.ServerEnum;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener {

    @EventHandler (priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();

        // teleport player to spawn
        player.teleport(Main.SPAWN);

        // broadcast join
        String message = "&f(&a+&f) &7{player} joined the server!".replace("{player}", event.getPlayer().getName());
        event.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));

        // set custom inventory
        player.getInventory().clear();
        player.getInventory().setStorageContents(InventoryBuilder.getPlayerInventory().getContents());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();

        // stop message
        event.setQuitMessage("");
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent event){
        Player player = event.getPlayer();

        if (event.getItemDrop().getItemStack().isSimilar(InventoryBuilder.getNavigatorItem())){
            event.setCancelled(true);

            if (player.isOp()){
                event.getItemDrop().getItemStack().setType(Material.AIR);
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        Player player = event.getPlayer();
        ItemStack item = player.getInventory().getItemInMainHand();

        if (item.isSimilar(InventoryBuilder.getNavigatorItem())){
            player.openInventory(InventoryBuilder.getNavigatorInventory());
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){
        Player player = (Player)event.getWhoClicked();
        ItemStack item = event.getCurrentItem();

        if (event.getClickedInventory() == null){
            return;
        }

        if (event.getClickedInventory().equals(InventoryBuilder.getNavigatorInventory())){
            event.setCancelled(true);

            if (event.getCurrentItem().getType() == null || !event.getCurrentItem().hasItemMeta()){
                return;
            }

            ServerEnum serverEnum = null;
            for (ServerEnum s : ServerEnum.values()){
                if (ChatColor.stripColor(s.getDisplayName()).equalsIgnoreCase(ChatColor.stripColor(item.getItemMeta().getDisplayName()))){
                    serverEnum = s;
                }
            }

            if (serverEnum != null){
                Main.connect(player, serverEnum.getServer());
                return;
            }
        }

    }
}
