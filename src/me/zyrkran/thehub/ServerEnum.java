package me.zyrkran.thehub;

import org.bukkit.ChatColor;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

public enum ServerEnum {

    LOBBY("&bLobby", new String[] {"Return to main lobby", "", "{players} currently playing!", "{server_status}"}, "LOBBY", Material.BOOKSHELF, 2),
    SURVIVAL("&bSurvival", new String[] {"Start a new adventure with friends", "or as a lone wolf. Build a market for profit and", "let em' know who's beast! Griefing is not allowed", "in this server.", "", "{players} currently playing!", "{server_status}"}, "SURVIVAL", Material.DIAMOND_PICKAXE, 4),
    CREATIVE("&bCreative", new String[] {"Build whatever you want without worrying about", "resources running out!", "", "{players} currently playing!", "{server_status}"}, "CREATIVE", Material.DIAMOND_BLOCK, 6);

    private String displayName;
    private String server;
    private String[] lore;
    private Material item;
    private int slot;

    ServerEnum(String displayName, String[] lore, String server, Material item, int slot){
        this.displayName = displayName;
        this.lore = lore;
        this.server = server;
        this.item = item;
        this.slot = slot;
    }

    public String getServer(){
        return server;
    }

    public String getDisplayName(){
        return color(displayName);
    }

    public List<String> getLore(){
        List<String> out = new ArrayList<>();
        for (String string : lore){
            string = string.replace("{players}", DataHandler.getPlayerCount(server) + "");
            string = string.replace("{server_status}", DataHandler.getServerStatus(server) ? "&a* &fClick to connect &a*" : "&cServer is currently offline!");
            out.add(color("&7" + string));
        }
        return out;
    }

    public Material getMaterial(){
        return item;
    }

    public int getSlot(){
        return slot;
    }

    private String color(String string){
        return ChatColor.translateAlternateColorCodes('&', string);
    }
}
