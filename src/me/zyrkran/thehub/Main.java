package me.zyrkran.thehub;

import me.zyrkran.thehub.commands.HubCommand;
import me.zyrkran.thehub.commands.ServersCommand;
import me.zyrkran.thehub.inventory.InventoryBuilder;
import me.zyrkran.thehub.listeners.PlayerListener;
import me.zyrkran.thehub.listeners.SocketManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{

    private static Main instance;
    public static Location SPAWN;
    public static InventoryBuilder BUILDER;

    public void onEnable(){
        instance = this;

        // init spawn
        SPAWN = new Location(Bukkit.getWorld("world"), 306.43742795380905, 112.0, 1478.4467194145568);
        SPAWN.setYaw(-269.10327F);
        SPAWN.setPitch(3.749838F);

        BUILDER = new InventoryBuilder();

        // register events
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        Bukkit.getPluginManager().registerEvents(new SocketManager(), this);

        // register commands
        getCommand("hub").setExecutor(new HubCommand());
        getCommand("servers").setExecutor(new ServersCommand());
    }

    public static void connect(Player player, String server){
        SocketManager.writeJSON("TheHub", "CONNECT_PLAYER_TO_SERVER;" + player.getUniqueId().toString()+ ";" + server);
    }

    public static Main getInstance(){
        return instance;
    }

}
