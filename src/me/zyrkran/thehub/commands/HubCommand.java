package me.zyrkran.thehub.commands;

import me.zyrkran.thehub.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by zyrkran on 07-10-17.
 */
public class HubCommand implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)){
            return false;
        }

        Player player = (Player) commandSender;

        if (args.length == 0){
            player.teleport(Main.SPAWN);
        }
        return false;
    }
}
